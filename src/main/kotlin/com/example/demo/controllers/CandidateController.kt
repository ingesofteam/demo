package com.example.demo.controllers

import com.example.demo.config.Routes
import com.example.demo.model.CreateCandidateRequest
import com.example.demo.model.entities.Candidate
import com.example.demo.services.CandidateService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.persistence.EntityNotFoundException

@RestController
@RequestMapping(Routes.CANDIDATE_PATH)
class CandidateController {

    @Autowired
    lateinit var candidateService: CandidateService

    @PostMapping
    fun create(@RequestBody request: CreateCandidateRequest) =
        candidateService.create(
            candidate = Candidate(name = request.name),
            partyId = request.partyId
        )

    @GetMapping(Routes.CANDIDATE_BY_ID)
    fun find(@PathVariable id: Long) = candidateService.find(id) ?: throw EntityNotFoundException("Candidate not found")

    @GetMapping
    fun getAll(@PageableDefault(size = 3)pageable: Pageable) = candidateService.getAll(pageable)
}
