package com.example.demo.controllers

import com.example.demo.config.Routes
import com.example.demo.model.entities.Party
import com.example.demo.services.CandidateService
import com.example.demo.services.PartyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.persistence.EntityNotFoundException

@RestController
@RequestMapping(Routes.PARTY_PATH)
class PartyController {

    @Autowired
    lateinit var partyService: PartyService

    @Autowired
    lateinit var candidateService: CandidateService

    @PostMapping
    fun create(@RequestBody party: Party) {
        partyService.create(party)
    }

    @GetMapping
    fun getAll() = partyService.findAll()

    @GetMapping(Routes.PARTY_BY_ID)
    fun findById(@PathVariable id: Long) =
        partyService.findById(id) ?: throw EntityNotFoundException("Party not exists")

    @GetMapping(Routes.FIND_CANDIDATES_BY_PARTY)
    fun findCandidatesByParty(@PathVariable partyId: Long, pageable: Pageable?) = candidateService.findByParty(partyId, pageable)
}
