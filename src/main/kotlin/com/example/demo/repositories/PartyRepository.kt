package com.example.demo.repositories

import com.example.demo.model.entities.Party
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface PartyRepository : PagingAndSortingRepository<Party, Long> {

    fun findByName(name: String): List<Party>
}
