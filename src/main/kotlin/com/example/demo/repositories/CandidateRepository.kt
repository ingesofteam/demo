package com.example.demo.repositories

import com.example.demo.model.entities.Candidate
import com.example.demo.model.entities.Party
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface CandidateRepository : PagingAndSortingRepository<Candidate, Long> {

    fun findByParty(party: Party, pageable:Pageable?): List<Candidate>

    override fun findAll(pageable:Pageable): Page<Candidate>
}
