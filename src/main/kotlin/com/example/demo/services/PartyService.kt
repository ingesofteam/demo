package com.example.demo.services

import com.example.demo.exceptions.BusinessException
import com.example.demo.model.entities.Party
import com.example.demo.repositories.PartyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PartyService {

    @Autowired
    lateinit var partyRepository: PartyRepository

    fun create(party: Party) {
        val partyFound = partyRepository.findByName(party.name)

        if (partyFound.isNotEmpty()) {
            throw BusinessException("Party already exists")
        }

        partyRepository.save(party)
    }

    fun findAll() = partyRepository.findAll()

    fun findById(id: Long) = partyRepository.findById(id).orElse(null)
}
