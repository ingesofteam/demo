package com.example.demo.services

import com.example.demo.externalapi.services.PartyMSService
import com.example.demo.model.entities.Candidate
import com.example.demo.model.entities.Party
import com.example.demo.repositories.CandidateRepository
import com.example.demo.repositories.PartyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class CandidateService {

    @Autowired
    lateinit var candidateRepository: CandidateRepository

    @Autowired
    lateinit var partyRepository: PartyRepository

    @Autowired
    lateinit var partyMSService: PartyMSService

    fun create(partyId: Long, candidate: Candidate) {
        val party = partyMSService.findParty(partyId.toString())

        if (party == null) {
            throw EntityNotFoundException("Party not present")
        }

        candidate.party = Party(party.id.toLong(), party.name)
        candidateRepository.save(candidate)
    }

    fun find(id: Long): Candidate? {
        return candidateRepository.findById(id).orElse(null)
    }

    fun findByParty(partyId: Long, pageable: Pageable?): List<Candidate> {
        val party = partyRepository.findById(partyId)

        if (!party.isPresent) {
            throw EntityNotFoundException("Party not present")
        }

        return candidateRepository.findByParty(party.get(), pageable)
    }

    fun getAll(pageable: Pageable) = candidateRepository.findAll(pageable)
}
