package com.example.demo.config

object Routes {
    const val PARTY_PATH = "/parties"
    const val CANDIDATE_PATH = "/candidates"

    const val FIND_CANDIDATES_BY_PARTY = "/{partyId}/candidates"
    const val CANDIDATE_BY_ID = "/{id}"
    const val PARTY_BY_ID = "/{id}"
}
