package com.example.demo.config

import feign.codec.Encoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.cloud.openfeign.support.SpringEncoder
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.converter.HttpMessageConverter

class FeignConfig {
    fun feignEncoder(): Encoder? {
        val jacksonConverter: HttpMessageConverter<*> = MappingJackson2HttpMessageConverter()
        val objectFactory: ObjectFactory<HttpMessageConverters> = ObjectFactory<HttpMessageConverters> {
            HttpMessageConverters(
                jacksonConverter
            )
        }
        return SpringEncoder(objectFactory)
    }
}
