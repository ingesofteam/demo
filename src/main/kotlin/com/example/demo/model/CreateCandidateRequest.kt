package com.example.demo.model

data class CreateCandidateRequest(
    val name: String,
    val partyId: Long
)
