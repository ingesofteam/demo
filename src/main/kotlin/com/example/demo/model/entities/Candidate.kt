package com.example.demo.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "candidate")
data class Candidate(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = 0L,

    @Column(name = "name")
    @NotEmpty
    @NotBlank
    var name: String,

    @ManyToOne
    @JoinColumn(name = "party_id")
    var party: Party? = null
)
