package com.example.demo.externalapi.clients

import com.example.demo.externalapi.model.responses.Party
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "PartyMS", url = "\${api.partyms.url}")
interface PartyMsClient {

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/parties/{id}"))
    fun getParty(@PathVariable id: String): Party
}