package com.example.demo.externalapi.model.responses

data class Party(
    val id: String,
    val name: String,
)
