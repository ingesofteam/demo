package com.example.demo.externalapi.model.responses

data class ErrorResponse(
    val status: Int,
    val message: String
)
