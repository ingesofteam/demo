package com.example.demo.externalapi.services

import com.example.demo.exceptions.BusinessException
import com.example.demo.externalapi.clients.PartyMsClient
import com.example.demo.externalapi.model.responses.ErrorResponse
import com.example.demo.externalapi.model.responses.Party
import com.fasterxml.jackson.databind.ObjectMapper
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component

@Component
class PartyMSService {

    @Autowired
    lateinit var partyMsClient: PartyMsClient

    @Autowired
    lateinit var objectMapper: ObjectMapper

    fun findParty(id: String): Party? {
        try {
        return partyMsClient.getParty(id)
        }catch (exc: FeignException) {
            val errorResponse = objectMapper.readValue(exc.contentUTF8(), ErrorResponse::class.java)

            throw BusinessException(errorResponse.message,
                HttpStatus.resolve(exc.status()) ?: HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}