CREATE TABLE candidate (
    id SERIAL PRIMARY KEY,
    name varchar(300) NOT NULL,
    party_id BIGINT NOT NULL
);

CREATE TABLE party (
    id SERIAL PRIMARY KEY,
    name varchar(300) NOT NULL
);
