package com.example.demo.controllers

import CommonTests
import com.example.demo.config.Routes
import com.example.demo.model.entities.Party
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class PartyControllerTests : CommonTests() {

    @Test
    fun createPartyTest() {
        val party = Party(1, "liberal")

        val request = MockMvcRequestBuilders
            .post(Routes.PARTY_PATH)
            .content(objectMapper.writeValueAsString(party))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val parties = partyRepository.findAll().asSequence().toList()

        val partyToAssert = parties.find { it.name == party.name }

        Assert.assertNotNull(partyToAssert)
        Assert.assertEquals(party.name, partyToAssert?.name)
    }

    @Test
    @Sql("/queries/partycontrollerTest/createPartyRepeatedName.sql")
    fun createPartyNameAlreadyTakenTest() {
        val party = Party(1, "liberal")

        val request = MockMvcRequestBuilders
            .post(Routes.PARTY_PATH)
            .content(objectMapper.writeValueAsString(party))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Party already exists")))
    }

    @Test
    @Sql("/queries/partycontrollerTest/createPartyRepeatedName.sql")
    fun createPartyRepeatedNameTest() {
        val party = Party(1, "liberal")

        val request = MockMvcRequestBuilders
            .post(Routes.PARTY_PATH)
            .content(objectMapper.writeValueAsString(party))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
    }

    @Test
    @Sql("/queries/partycontrollerTest/findParty.sql")
    fun findPartyTest() {
        val partyId = 1L

        val request = MockMvcRequestBuilders
            .get("${Routes.PARTY_PATH}/${Routes.PARTY_BY_ID}", partyId)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.`is`("liberal")))
    }

    @Test
    @Sql("/queries/partycontrollerTest/findCandidateByParty.sql")
    fun findCandidatesByPartyTest() {
        val partyId = 1L

        val request = MockMvcRequestBuilders
            .get("${Routes.PARTY_PATH}/${Routes.FIND_CANDIDATES_BY_PARTY}", partyId)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.`is`("Pepe")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.`is`("Jose")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", Matchers.`is`("Luis")))
    }

    @Test
    @Sql("/queries/partycontrollerTest/findCandidateByParty.sql")
    fun findCandidatesByPartyNotFoundPartyTest() {
        val partyId = 3L

        val request = MockMvcRequestBuilders
            .get("${Routes.PARTY_PATH}/${Routes.FIND_CANDIDATES_BY_PARTY}", partyId)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Party not present")))
    }
}
